Панель продуктов
=============================

Установка
------------

Команды для запуска проекта:
-----------
    Скопировать .env.example и переименовать в .env
    
    docker-compose build --build-arg uid=$(id -u)               - Собрать окружение           
    docker-compose up -d                                        - Запустить контейнеры
    cd storage/
    mkdir -p framework/{sessions,views,cache}    
    chmod -R 777 framework
    docker-compose exec rolex composer install                  - Установить зависимости 
    docker-compose run --rm nodejs npm i                        - Установить js зависимости 
    docker-compose run --rm nodejs npm run dev                  - Сборка ресурсов 
    docker-compose exec rolex php artisan migrate               - Накатить миграции
    docker-compose exec rolex php artisan storage:link          - Создание симлинка для публичного доступа к файлам, хранимым в storage/app/public

Команды
-----------
    sudo chown -R 1000:1000 database/migrations                             - Дать права на папку
    docker-compose run --rm nodejs npm run dev                              - Собрать ресурсы и запустить watch    
    docker-compose exec rolex php artisan migrate                           - Накатить миграции
    docker-compose exec rolex php artisan make:migration <name migration>   - Создать новую миграцию

Редис
----------- 
    docker-compose exec redis redis-cli                                     - Войти в консоль редис        
    auth dev                                                                - Авторизоваться в redis
    docker-compose exec rolex php artisan job:execute > /dev/null &         - Запуск демона для выполнения заданий из очереди execute:
    docker-compose exec -T rolex sh scripts/restart_jobs_deamon.sh          - Убить и перезапустить процессы заданий -  скрипт для локалки

Супервизор для демонов
----------- 
    Конфиги и скрипт демона
    nano /etc/supervisor/conf.d/rolex.conf
    nano /etc/supervisor/conf.d/rolex.sh
    
    вебинтерфейс
    http://89.108.64.2:9001/
    user - adminpavel
    pass - 1drtgsb0_3

    service supervisor restart
    supervisorctl reread
    supervisorctl update    
    supervisorctl status
    supervisorctl start all
    supervisorctl stop all
    supervisorctl start <name>
    supervisorctl stop <name>

Правила наименования веток и коммитов
-----------
    Работа по провекту ведется по задачам из трелло
    Вам нужно установить расширение в свой браузер - https://chrome.google.com/webstore/detail/trello-card-numbers/kadpkdielickimifpinkknemjdipghaf/related
    Если у вас крупная задача в несколько коммитов,  cоздаем ветку от release-*-stable и называем ее #НОМЕРКАРТОЧКИ_название_ветки
    В ней делаем коммиты тоже с названием #НОМЕРКАРТОЧКИ название коммита
    
    Если прилетела задача типа бага или фичи небольшой
    Просто в release-*-stable делаем коммит с названием #НОМЕРКАРТОЧКИ название коммита

Хосты
-----------
Сгенерировать сертификат по гайду

[Проект прокси](https://gitlab.com/ingrup_products/proxy/-/blob/master/README.md)

    127.0.0.1 rolex.local.dv
